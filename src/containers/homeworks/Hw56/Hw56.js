import OrderDetails from "./components/OrderDetails/OrderDetails";
import ItemsList from "./components/ItemsList/ItemsList";
import {nanoid} from "nanoid";
import {useState} from "react";
import './Hw56.css';

const Hw56 = () => {
   const initArr = [
      {name: 'Hamburger', price: 80, currency: 'kgs', amount: 0, id: nanoid()},
      {name: 'Cheeseburger', price: 90, currency: 'kgs', amount: 0, id: nanoid()},
      {name: 'Fries', price: 45, currency: 'kgs', amount: 0, id: nanoid()},
      {name: 'Coffee', price: 70, currency: 'kgs', amount: 0, id: nanoid()},
      {name: 'Tea', price: 50, currency: 'kgs', amount: 0, id: nanoid()},
      {name: 'Cola', price: 40, currency: 'kgs', amount: 0, id: nanoid()},
   ];
   const [items, setItems] = useState(initArr);

   const addItem = (id) => {
      setItems([...items.map((item) => {
         if (id === item.id) {
            item.amount++;
            return item;
         }
         return item;
      })])
   }

   const removeItem = (id) => {
      setItems([...items.map((item) => {
         if (id === item.id) {
            item.amount = 0;
            return item;
         }
         return item;
      })])
   }

   return (
     <div className="Hw56">
        <ItemsList items={items} addItem={addItem}/>
        <OrderDetails items={items} removeItem={removeItem}/>
     </div>
   );
}

export default Hw56;
