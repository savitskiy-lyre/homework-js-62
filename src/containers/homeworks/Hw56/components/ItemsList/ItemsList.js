import React from 'react';
import Item from "./Item/Item";
import './ItemList.css';
import {nanoid} from "nanoid";

const ItemsList = ({items, addItem}) => {
   return (
     <fieldset style={{marginRight: '10px'}}>
        <legend>Add Items:</legend>
        <div className="items-wrapper">
        {items.map((item) => {
           return (
             <Item
               name={item.name}
               price={item.price}
               currency={item.currency}
               id={item.id}
               key={nanoid()}
               addItem={() => addItem(item.id)}
             />)
        })}
        </div>
     </fieldset>
   );
};

export default ItemsList;