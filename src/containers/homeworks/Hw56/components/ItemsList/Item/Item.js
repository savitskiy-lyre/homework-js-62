import React from 'react';
import './Item.css';

const Item = (props) => {
   return (
     <div className="item-wrapper" onClick={props.addItem}>
        <div className={"img " + props.name}/>
        <div className="item-info">
        <p><strong>{props.name}</strong></p>
        <p>Price: {props.price + ' ' + props.currency.toUpperCase()}</p>
        </div>
     </div>
   );
};

export default Item;