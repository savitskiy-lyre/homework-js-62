import './Hw59.css';
import React from 'react';
import WishFilms1 from "./components/WishFilms1/WishFilms1";
import WishFilms2 from "./components/WishFilms2/WishFilms2";
import ChuckJokes from "./components/ChuckJokes/ChuckJokes";


function Hw59() {

   return (
     <div className="Hw59">
        <WishFilms1/>
        <ChuckJokes/>
        <WishFilms2/>
     </div>
   );
}

export default Hw59;