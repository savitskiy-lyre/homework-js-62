import React, {Component} from 'react';

class FilmForm extends Component {
   state = {value: ''};

   onFormSubmit = (e) => {
      e.preventDefault();
      this.props.addNewFilm(this.state.value);
      this.setState({value: ''});
   };

   onInputChange = (e) => {
      this.setState({value: e.target.value});
   };

   shouldComponentUpdate(nextProps, nextState) {
      return this.state.value !== nextState.value;
   };

   render() {
      return (
        <form onSubmit={this.onFormSubmit}>
           <input type="text" value={this.state.value} onChange={this.onInputChange} required/>
           <button>Add</button>
        </form>
      );
   };
}

export default FilmForm;