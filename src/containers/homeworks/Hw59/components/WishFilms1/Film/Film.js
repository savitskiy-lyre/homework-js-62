import React, {Component} from 'react';
import './Film.css';

class Film extends Component {
   shouldComponentUpdate(nextProps) {
      return this.props.text !== nextProps.text;
   }

   render() {
      return (
        <div className="film-wrapper">
           <input type="text" onChange={this.props.onInputChange} value={this.props.text}/>
           <button onClick={this.props.removeFilm}>X</button>
        </div>
      );
   }
}

export default Film;