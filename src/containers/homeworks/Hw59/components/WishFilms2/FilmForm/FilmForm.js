import React, {useState} from 'react';

const FilmForm = ({addNewFilm}) => {
   const [inpValue, setInpValue] = useState('');

   const onFormSubmit = (e) => {
      e.preventDefault();
      addNewFilm(inpValue);
      setInpValue('');
   };

   const onInputChange = (e) => {
      setInpValue(e.target.value);
   };

   return (
     <form onSubmit={onFormSubmit}>
        <input type="text" value={inpValue} onChange={onInputChange} required/>
        <button>Add</button>
     </form>
   );
};

export default FilmForm;
