import './Hw58.css';
import React, {useState} from "react";
import Modal from "./components/UI/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";
import {nanoid} from "nanoid";

const initAlerts = [
   {
      coordinates: ['15px', '20px'],
      type: "success",
      key: nanoid(),
      id: nanoid(),
      show: true,
   },
   {
      coordinates: ['15px', '80px'],
      dismissed: true,
      type: "primary",
      key: nanoid(),
      id: nanoid(),
      show: true,
   },
   {
      coordinates: ['15px', '140px'],
      dismissed: true,
      type: "danger",
      key: nanoid(),
      id: nanoid(),
      show: true,
   },
   {
      coordinates: ['15px', '200px'],
      type: "warning",
      key: nanoid(),
      id: nanoid(),
      show: true,
   },
]

const Hw58 = () => {
   const [showModal, setShowModal] = useState(false);
   const [alerts, setAlerts] = useState(initAlerts);
   const alertDismiss = (id) => {
      setAlerts((prev) => {
         return prev.map((alert) => {
            if (id === alert.id) {
               alert.show = !alert.show;
            }
            return alert;
         })
      })
   };

   const onChangeModal = () => {
      setShowModal(!showModal);
   }
   const showAlertHandlers = []

   return (
     <div className="Hw58">
        {alerts.map((alert) => {
           showAlertHandlers.push(() => alertDismiss(alert.id));
           if (alert.show) {
              return (
                <Alert
                  coordinates={alert.coordinates}
                  dismissed={alert.dismissed}
                  type={alert.type}
                  dismiss={() => alertDismiss(alert.id)}
                  key={alert.key}
                >
                   Hey
                </Alert>
              );
           }
           return null;
        })}
        <Modal
          show={showModal}
          closed={onChangeModal}
          title={"Title"}
          btnArr={[
             {
                type: 'primary',
                label: 'Primary',
                clicked: showAlertHandlers[0],
             },
             {
                type: 'danger',
                label: 'Danger',
                clicked: showAlertHandlers[1],
             }, {
                type: 'success',
                label: 'Success',
                clicked: showAlertHandlers[2],
             }, {
                type: 'warning',
                label: 'Warning',
                clicked: showAlertHandlers[3],
             },
          ]}
        >
           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae corporis eum iure laborum quam qui? Adipisci
           dolores doloribus ducimus error nam. A aliquam libero officiis quaerat. Esse maiores nemo sit?
        </Modal>
        <button onClick={onChangeModal} style={{margin: '10px', padding: '5px 10px'}}>Test</button>
     </div>
   );
};

export default Hw58;
