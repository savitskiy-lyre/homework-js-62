import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = (props) => {
   return (
     <>
        <Backdrop show={props.show} onClick={props.closed}/>
        <div
          className="Modal"
          style={{
             transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
             opacity: props.show ? '1' : '0'
          }}
        >
           <div className="modal-top">
              {props.title ? <h4 className="modal-title">{props.title}</h4> : null}
              <button onClick={props.closed} className="close-btn">X</button>
           </div>
           <div className="modal-content">
              {props.children}
           </div>
           {props.btnArr ? (
             <div className="modal-btns">
                {props.btnArr.map((btn, i) => {
                   return (<button className={"btns "+btn.type} onClick={btn.clicked} key={btn.type + btn.label + i}>{btn.label}</button>)
                })}

             </div>
           ) : null}

        </div>
     </>
   );
};

export default Modal;