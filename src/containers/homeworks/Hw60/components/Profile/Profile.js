import React from 'react';
import Modal from "../UI/Modal/Modal";

const Profile = ({profile, closeProfile, openedProfile, openProfile}) => {
   return (
     <div className="d-flex align-items-center">
        {profile ? <span>{profile}</span> : <div className="preloader"/>}
        <button className="btn btn-secondary ms-auto" onClick={openProfile}>Redact</button>
        <Modal
          show={openedProfile}
          closed={closeProfile}
          title={"Oops..."}
        />
     </div>
   );
};

export default Profile;