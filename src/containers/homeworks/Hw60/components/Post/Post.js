import React from 'react';

const Post = ({post}) => {
   return (
     <div className="post">
        <strong>{post.author} said:</strong>
        <div className="my-2">{post.message}</div>
        <i className="post-time">{post.datetime}</i>
     </div>
   );
};

export default React.memo(Post);