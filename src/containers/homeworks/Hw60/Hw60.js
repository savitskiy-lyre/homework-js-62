import axios from 'axios'
import {useEffect, useState} from "react";
import './Hw60.css';
import Post from "./components/Post/Post";
import FormPost from "./components/FormPost/FormPost";
import Profile from "./components/Profile/Profile";

const url = 'http://146.185.154.90:8000/messages';
const Hw60 = () => {

   const [openedProfile, setOpenedProfile] = useState(false);
   const [profile, setProfile] = useState(null);
   const [posts, setPosts] = useState(null);
   const [postInp, setPostInp] = useState('');

   const openProfile = () => {
      setOpenedProfile(true);
   };

   const closeProfile = () => {
      setOpenedProfile(false);
   };

   useEffect(() => {
      if (posts !== null) {
         const requestIntervalPosts = async () => {
            const {data} = await axios.get(url + '?datetime=' + posts[posts.length - 1]['datetime'])
            //console.log(data)
            if (data.length > 0) {
               setPosts((prev) => {
                  return [...prev].concat(data);
               })
            }
         }
         const interval = setInterval(requestIntervalPosts, 3000);
         return () => {
            clearInterval(interval);
         }
      } else {
         const requestProfile = async () => {
            const {data: profile} = await axios.get('http://146.185.154.90:8000/blog/dumaedzz@gmail.com/profile');
            const {data: posts} = await axios.get(url);
            setPosts(posts);
            setProfile(profile.firstName + ' ' + profile.lastName);
         };
         requestProfile();
      }
   }, [posts]);


   const onMessageSubmit = async (e) => {
      e.preventDefault();
      const data = new URLSearchParams();
      data.set('message', postInp);
      data.set('author', profile);
      await axios.post(url, data);
      setPostInp('')
   };

   const onChangePostInp = (e) => {
      setPostInp(e.target.value);
   };

   return (
     <div className="Hw60">
        <Profile
          profile={profile}
          closeProfile={closeProfile}
          openedProfile={openedProfile}
          openProfile={openProfile}
        />
        <FormPost
          onMessageSubmit={onMessageSubmit}
          onChangePostInp={onChangePostInp}
          postInp={postInp}
        />
        <div className="posts">
           {posts ? posts.map((post) => <Post key={post._id} post={post}/>).reverse() :
             <div className="posts-preloader"/>}
        </div>
     </div>
   );
};

export default Hw60;
