import React from 'react';

const ResetBtn = ({onResetBtn}) => {
   return (
     <div><button onClick={onResetBtn}>Reset</button></div>
   );
};

export default ResetBtn;