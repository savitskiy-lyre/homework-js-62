import React, {useState} from "react";
import {nanoid} from "nanoid";
import PlayingField from "./components/PlayingField/PlayingField";
import Tries from "./components/Tries/Tries";
import ResetBtn from "./components/ResetBtn/ResetBtn";
import './Hw54.css';

const Hw54 = () => {
   const createFields = () => {
      const fields = [];
      for (let i = 0; i < 36; i++) {
         fields.push({
            isFlip: false,
            id: nanoid(),
            hasItem: false,
         });
      }
      fields[Math.floor(Math.random() * 36)].hasItem = true;
      return fields;
   }

   const [fieldsState, setFieldsState] = useState(createFields());
   const [count, SetCount] = useState(0);

   const onResetBtn = () => {
      setFieldsState(createFields());
      SetCount(0);
   };

   const onOpenField = (id) => {
      setFieldsState(fieldsState.map((field) => {
               if (field.id === id) {
                  if (!field.isFlip ){
                     SetCount(count + 1)
                     field.isFlip = !field.isFlip;
                  }
         }
         return field;
      }))
   }

   return (
     <div className="Hw54">
        <PlayingField onOpenField={onOpenField} fieldsState={fieldsState}/>
        <Tries count={count}/>
        <ResetBtn onResetBtn={onResetBtn}/>
     </div>
   )
};

export default Hw54;
