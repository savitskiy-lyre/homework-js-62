import React, {Component} from 'react';
import Card from "./Card/Card";
import './CardDeck.css'
import PokerHand from "./PokerHand/PokerHand";

class CardDeck extends Component {
   constructor() {
      super();
      this.needToChangeCards = [];
      if (!this.state) {
         this.state = {
            changeBtnDisable: false,
            fiveCards: this.createDeck(),
         };
      }
   };

   flipChanger = () => {
      return this.state.changeBtnDisable;
   };

   createDeck = () => {
      const allCards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
      const allSuits = ['S', 'H', 'C', 'D'];

      const getFiveCards = () => {
         const fiveCards = this.getCards();
         this.needToChangeCards = [];
         return fiveCards.map((card, index) => {
            return (
              <Card
                rank={card.rank}
                suit={card.suit}
                key={card.key}
                cardIndex={index}
                switcher={this.switcher}
                flipChanger={this.flipChanger}
              />);
         });
      };

      if (!this.allDeck || this.allDeck.length < 4) {
         this.allDeck = [];
         for (const suit of allSuits) {
            for (const card of allCards) {
               this.allDeck.push({
                  rank: card,
                  suit: suit,
                  key: card + suit,
               })
            }
         }
         return getFiveCards();
      }

      let change = false;
      for (const card of this.needToChangeCards) {
         if (card) {
            change = true;
            break;
         }
      }
      if (change) {
         const changedState = [...this.state.fiveCards];
         for (const index of this.needToChangeCards) {
            if (index) {
               const card = this.getCard();
               changedState[index] = (
                 <Card
                   rank={card.rank}
                   suit={card.suit}
                   key={card.key}
                   cardIndex={parseInt(index)}
                   switcher={this.switcher}
                   flipChanger={this.flipChanger}
                 />);
            }
         }
         this.needToChangeCards = [];
         return changedState;
      } else {
         return getFiveCards();
      }
   };

   switcher = (index) => {
      index += '';
      if (!this.needToChangeCards[index]) {
         this.needToChangeCards[index] = index;
      } else {
         this.needToChangeCards[index] = null;
      }
   }

   getCard = () => {
      return this.allDeck.splice(Math.floor(Math.random() * this.allDeck.length), 1)[0];
   };

   getCards = (howMany = 5) => {
      const cardsArr = [];
      for (let i = 0; i < howMany; i++) {
         cardsArr.push(this.getCard());
      }
      return cardsArr;
   };

   changeCards = () => {
      if (!this.state.changeBtnDisable) {
         this.setState({changeBtnDisable: true});
      }
      this.setState({fiveCards: this.createDeck()});
   };

   newGame = () => {
      this.allDeck = null;
      if (this.state.changeBtnDisable) {
         this.setState({changeBtnDisable: false});
      }
      this.setState({fiveCards: this.createDeck()});
   };

   render() {
      return (
        <div className="cardDeckWrapper">
           <div>
              <button onClick={this.changeCards} disabled={this.state.changeBtnDisable}>Change Cards</button>
              <button onClick={this.newGame}>New Game</button>
           </div>
           <div className="cardsWrapper">
              {this.state.fiveCards}
           </div>
           <PokerHand state={this.state}/>
        </div>
      );
   }
}

export default CardDeck;