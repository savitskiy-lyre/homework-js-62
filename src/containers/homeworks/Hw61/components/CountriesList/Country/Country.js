import React from 'react';

const Country = ({countryClassName, onClickCountry, id, name}) => {
   return (
     <p
       className={countryClassName}
       onClick={onClickCountry}
       key={id}>
        {name}
     </p>
   );
};

export default React.memo(Country, (prev, next) => {
   return prev.countryClassName === next.countryClassName;
});