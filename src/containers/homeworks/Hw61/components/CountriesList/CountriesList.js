import React from 'react';
import './CountriesList.css';
import Country from "./Country/Country";

const CountriesList = ({countries, onClickCountry, selectedCountry}) => {

   return (
     <div className="countries-list">
        <div className="p-2 text-center">Countries:</div>
        {countries ? countries.map((country) => {
           let countryClassName = 'country';
           if (countries && selectedCountry) {
              countryClassName = selectedCountry.id === country.id ? countryClassName + ' active' : countryClassName;
           }
           return (
             <Country
               countryClassName={countryClassName}
               onClickCountry={() => onClickCountry(country)}
               name={country.name}
               key={country.id}
             />)
        }) : <div className="preloader m-auto" style={{width: '50px', height: '50px'}}/>}
     </div>
   );
};

export default CountriesList;