import React, {useMemo} from 'react';
import './CountryInfo.css';

const CountryInfo = ({selectedCountry, countryInfo, countryRequestBeen}) => {
   let countryInfoEl = useMemo(() => {
      if (countryInfo) {
         return (
           <div className="d-flex flex-column w-100 text-center">
              <img src={countryInfo.flag} className="mx-auto mb-3" alt="flag"
                   style={{width: '300px', height: '200px'}}/>
              <p><strong>Name: </strong>{countryInfo.name}</p>
              <p><strong>Region: </strong>{countryInfo.region}</p>
              <p><strong>Capital: </strong>{countryInfo.capital}</p>
              <p><strong>Population: </strong>{countryInfo.population}</p>
              <div className="d-flex justify-content-evenly">
                 <div className="text-start p-2">
                    <strong>Translations: </strong>
                    <ul>{Object.keys(countryInfo.translations).map((key) => (
                      <li key={countryInfo.id + key}>
                         {key + ': ' + countryInfo['translations'][key]}
                      </li>)
                    )}
                    </ul>
                 </div>
                 <div className="text-start p-2">
                    <strong>Borders: </strong>
                    <ul>{countryInfo.borders.length === 0 ? <li>None</li> : countryInfo.borders.map((name) => (
                      <li key={Math.random()}>
                         {name}
                      </li>
                    ))}
                    </ul>
                 </div>
              </div>
           </div>
         );
      }
      return null;
   }, [countryInfo]);

   return (
     <div className="country-info">
        {!selectedCountry ? <p className="m-auto fs-1">Choose country please !?!?!</p> : null}
        {countryRequestBeen ? <div className="preloader m-auto" style={{width: '150px', height: '150px'}}/> : null}
        {countryInfoEl}
     </div>
   );
};

export default CountryInfo;