import './Hw61.css';
import CountriesList from "./components/CountriesList/CountriesList";
import CountryInfo from "./components/CountryInfo/CountryInfo";
import {useEffect, useState} from "react";
import axios from "axios";
import {BASE_URL, COUNTRIES_URL, COUNTRY_3CODE_URL} from "../../../config";

axios.defaults.baseURL = BASE_URL;

const Hw61 = () => {
   const [countries, setCountries] = useState(null);
   const [selectedCountry, setSelectedCountry] = useState(null);
   const [countryInfo, setCountryInfo] = useState(null);
   const [countryRequestBeen, setCountryRequestBeen] = useState(false);
   const onClickCountry = (country) => {
      setCountryInfo(null);
      setCountryRequestBeen(true);
      setSelectedCountry(country);
   };

   useEffect(() => {
      const requestCounties = async () => {
         const {data} = await axios(COUNTRIES_URL);
         setCountries(data.map(country => ({...country, id: Math.random()})));
      }
      requestCounties()
   }, []);

   useEffect(() => {
      if (selectedCountry !== null ) {
         const requestCountryInfo = async () => {
            const {data} = await axios(COUNTRY_3CODE_URL + selectedCountry.alpha3Code);
            const bordersRequestsArr = [];
            for (let i = 0; i < data.borders.length; i++) {
               bordersRequestsArr.push(axios(COUNTRY_3CODE_URL + data.borders[i]));
            }
            const countryBordersResponses = await Promise.all(bordersRequestsArr);
            data.borders = countryBordersResponses.map((response) => response.data.name);
            setCountryInfo(data);
            setCountryRequestBeen(false);
         };
         requestCountryInfo();
      }
   }, [selectedCountry])
   return (
     <div className="Hw61 d-flex py-2">
           <CountriesList
             countries={countries}
             onClickCountry={onClickCountry}
             selectedCountry={selectedCountry}/>
           <CountryInfo
             selectedCountry={selectedCountry}
             countryInfo={countryInfo}
             countryRequestBeen={countryRequestBeen}/>
     </div>
   );
};

export default Hw61;
