import Burger from "./components/Burger/Burger";
import Ingredients from "./components/Ingredients/Ingredients";
import './Hw55.css';
import {nanoid} from 'nanoid';
import {useState} from "react";


const Hw55 = () => {
   const initialState = [
      {name: 'Meat', amount: 1, id: nanoid(), key: nanoid(),},
      {name: 'Cheese', amount: 1, id: nanoid(), key: nanoid(),},
      {name: 'Salad', amount: 1, id: nanoid(), key: nanoid(),},
      {name: 'Bacon', amount: 1, id: nanoid(), key: nanoid(),},
   ];
   const [ingredients, setIngredients] = useState(initialState);

   const decreaseAmount = (id) => {
      setIngredients(ingredients.map((ingredient) => {
         if ((ingredient.id === id) && ingredient.amount > 0) {
            ingredient.amount--;
            return ingredient;
         }
         return ingredient;
      }))
   };
   const increaseAmount = (id) => {
      setIngredients(ingredients.map((ingredient) => {
         if ((ingredient.id === id) && ingredient.amount < 5) {
            ingredient.amount++
            return ingredient;
         }
         return ingredient;
      }))

   };
   const resetAmount = (id) => {
      setIngredients(ingredients.map((ingredient) => {
         if ((ingredient.id === id) && ingredient.amount !== 0) {
            ingredient.amount = 0;
            return ingredient;
         }
         return ingredient;
      }))

   };

   return (
     <div className="Hw55">
        <Ingredients
          ingredients={ingredients}
          decreaseAmount={decreaseAmount}
          increaseAmount={increaseAmount}
          resetAmount={resetAmount}
        />
        <Burger ingredients={ingredients}/>
     </div>
   );
}

export default Hw55;
