import React from 'react';
import './Burger.css';

const priceList = {
   base: 20,
   cheese: 20,
   meat: 50,
   salad: 5,
   bacon: 30,
}

const Burger = ({ingredients}) => {
   let price = priceList.base;
   const dataIng = ingredients.map((ingredient) => {
      price += priceList[ingredient.name.toLowerCase()] * ingredient.amount;
      return {name: ingredient.name, amount: ingredient.amount};
   })

   const burgerEls = [];
   dataIng.forEach((ing) => {
      if (ing.amount > 0) {
         for (let i = 0; i < ing.amount; i++) {
            burgerEls.push(<div key={Math.random()} className={ing.name}/>)
         }
      }
   })

   return (
     <fieldset className="wrapper">
        <legend>Burger</legend>
        <div className="Burger">
           <div className="BreadTop">
              <div className="Seeds1"/>
              <div className="Seeds2"/>
           </div>
           {burgerEls}
           <div className="BreadBottom"/>
        </div>
        <p>Price: {price}</p>
     </fieldset>
   );
};

export default Burger;