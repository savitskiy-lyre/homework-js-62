import React from 'react';
import Ingredient from "./Ingredient/Ingredient";

const Ingredients = ({ingredients, increaseAmount, decreaseAmount,resetAmount}) => {
   ingredients = ingredients.map((ingredient) => {
      return (
        <Ingredient
          name={ingredient.name}
          amount={ingredient.amount}
          key={ingredient.key}
          decreaseAmount={() => decreaseAmount(ingredient.id)}
          increaseAmount={() => increaseAmount(ingredient.id)}
          resetAmount={() => resetAmount(ingredient.id)}
        />)
   });
   return (
     <fieldset>
        <legend>Ingredients</legend>
        {ingredients}
     </fieldset>
   );
};

export default Ingredients;