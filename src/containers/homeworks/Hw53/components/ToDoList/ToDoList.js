import FormTask from "./FormTask/FormTask";
import Task from "./Task/Task";
import './ToDoList.css';
import {useState} from "react";
import {nanoid} from 'nanoid';

const ToDoList = () => {
   const [tasks, setTasks] = useState([
      {message: 'Sell milk', id: nanoid(), completed: true},
      {message: 'Buy water', id: nanoid(), completed: false},
      {message: 'Walk with dog', id: nanoid(), completed: true},
      {message: 'Do homework', id: nanoid(), completed: false},]);

   const [currentTask, setCurrentTask] = useState('');

   const onTextChange = (e) => {
      setCurrentTask(e.target.value);
   };

   const onSubmitTask = (e)=>{
      e.preventDefault();
      setTasks([...tasks, {message: currentTask, id: nanoid(), completed: false},]);
      setCurrentTask('');
   };

   const tasksEl = tasks.map((task) => {
      const onCompleted = (id) => {
         const changedTasks = [...tasks];
         setTasks(changedTasks.map((task) => {
            if (task.id === id) {
               task.completed = !task.completed;
            }
            return task

         }));
      };

      const onRemoved = (id) => {
         const changedTasks = [...tasks];
         const neededTasks = changedTasks.filter((task) => {
            if (task.id !== id) {
               return task;
            }
            return null;
         })
         setTasks(neededTasks);
      }

      return (
        <Task message={task.message}
              key={nanoid()}
              id={task.id}
              onRemoved={() => onRemoved(task.id)}
              completed={task.completed}
              onCompleted={() => onCompleted(task.id)}
        />)
   });
   return (
     <div className="toDoList-wrapper">
        <FormTask
          currentTask={currentTask}
          onTextChange={onTextChange}
          onSubmitTask={onSubmitTask}/>
        {tasksEl}
     </div>
   );
};

export default ToDoList;