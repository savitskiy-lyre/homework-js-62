import './FormTask.css'

const FormTask = (props) => {
   return (
     <form className="formTask-wrapper" onSubmit={props.onSubmitTask}>
        <input type="text" placeholder="Add new task" value={props.currentTask} onChange={props.onTextChange} required/>
        <button>Add</button>
     </form>
   );
};

export default FormTask;