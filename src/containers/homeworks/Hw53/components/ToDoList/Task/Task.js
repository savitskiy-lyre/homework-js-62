import './Task.css';

const Task = (props) => {
   let checked = false;
   let classNames = "task-wrapper clearfix ";
   if (props.completed) {
      checked = true;
      classNames += "task-completed";
   }
   return (
     <div className={classNames}>
        <div className="btns">
           <input type="checkbox" checked={checked} onChange={props.onCompleted}/>
           <button onClick={props.onRemoved}>X</button>
        </div>
        <div className="message">
           {props.message}
        </div>

     </div>
   );
};

export default Task;