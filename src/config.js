export const BASE_URL = 'https://restcountries.eu/';
export const COUNTRIES_URL = 'rest/v2/all';
export const COUNTRY_3CODE_URL = 'rest/v2/alpha/';