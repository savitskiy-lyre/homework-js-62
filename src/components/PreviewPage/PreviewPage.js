import React from 'react';

const PreviewPage = () => {
   return (
     <div style={{display:'flex', minHeight: 'auto'}}>
        <div style={{margin: '200px auto', fontSize: '60px'}}>
           Welcome !
        </div>
     </div>
   );
};

export default PreviewPage;