import React from 'react';
import './NavBar.css';
import {NavLink} from "react-router-dom";

const NavBar = () => {
   const arr = []
   for (let i = 51; i <= 61; i++) {
      const path = '/homework/' + i;
      arr.push(<NavLink className="NavBar-item" to={path} key={Math.random()}>{i}</NavLink>);
   }
   return (
     <div className="NavBar-wrapper">
        <h2>Homework's examples:</h2>
        <div className="NavBar-items-wrapper">
           {arr}
        </div>
     </div>
   );
};

export default NavBar;
