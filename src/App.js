import React from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import NavBar from "./components/Navbar/NavBar";
import PreviewPage from "./components/PreviewPage/PreviewPage";
import Hw51 from "./containers/homeworks/Hw51/Hw51";
import Hw52 from "./containers/homeworks/Hw52/Hw52";
import Hw53 from "./containers/homeworks/Hw53/Hw53";
import Hw54 from "./containers/homeworks/Hw54/Hw54";
import Hw55 from "./containers/homeworks/Hw55/Hw55";
import Hw56 from "./containers/homeworks/Hw56/Hw56";
import Hw57 from "./containers/homeworks/Hw57/Hw57";
import Hw58 from "./containers/homeworks/Hw58/Hw58";
import Hw59 from "./containers/homeworks/Hw59/Hw59";
import Hw60 from "./containers/homeworks/Hw60/Hw60";
import Hw61 from "./containers/homeworks/Hw61/Hw61";

function App() {
   return (
     <div className="App">
        <NavBar/>
        <div className="homework-wrapper">
           <Switch>
              <Route path='/' exact component={PreviewPage}/>
              <Route path='/homework/51' component={Hw51}/>
              <Route path='/homework/52' component={Hw52}/>
              <Route path='/homework/53' component={Hw53}/>
              <Route path='/homework/54' component={Hw54}/>
              <Route path='/homework/55' component={Hw55}/>
              <Route path='/homework/56' component={Hw56}/>
              <Route path='/homework/57' component={Hw57}/>
              <Route path='/homework/58' component={Hw58}/>
              <Route path='/homework/59' component={Hw59}/>
              <Route path='/homework/60' component={Hw60}/>
              <Route path='/homework/61' component={Hw61}/>
           </Switch>
        </div>

     </div>
   );
}

export default React.memo(App);
